-- Insert data into arq_per_db.persona
INSERT INTO persona (cc, nombre, apellido, genero, edad) VALUES
(1001, 'Juan', 'Pérez', 'M', 35),
(1002, 'María', 'Gómez', 'F', 28),
(1003, 'Carlos', 'Rodríguez', 'M', 42),
(1004, 'Laura', 'Hernández', 'F', 25),
(1005, 'Andrés', 'Sánchez', 'M', 30);

-- Insert data into arq_per_db.profesion
INSERT INTO profesion (id, nom, des) VALUES
(1, 'Ingeniero de Sistemas', 'Especialista en desarrollo de software'),
(2, 'Médico', 'Especialista en atención de la salud'),
(3, 'Abogado', 'Especialista en asesoría legal'),
(4, 'Psicólogo', 'Especialista en atención psicológica');

-- Insert data into arq_per_db.estudios
INSERT INTO estudios (id_prof, cc_per, fecha, univer) VALUES
(1, 1001, '2006-07-01', 'Universidad Nacional'),
(1, 1002, '2010-06-01', 'Universidad de los Andes'),
(2, 1003, '2004-12-01', 'Universidad del Rosario'),
(3, 1004, '2012-01-01', 'Universidad Externado de Colombia'),
(4, 1005, '2010-09-01', 'Universidad Javeriana');

-- Insert data into arq_per_db.telefono
INSERT INTO telefono (num, oper, duenio) VALUES
('3101234567', 'Claro', 1001),
('3009876543', 'Movistar', 1002),
('3154567890', 'Tigo', 1003),
('3107654321', 'Claro', 1004),
('3202345678', 'Movistar', 1005);