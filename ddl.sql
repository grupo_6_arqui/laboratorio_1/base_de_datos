GO

IF DB_ID('arq_per_db') IS NOT NULL
BEGIN
    ALTER DATABASE arq_per_db SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE arq_per_db;
END
GO

CREATE DATABASE arq_per_db;
GO

USE arq_per_db;
GO

-- Table `arq_per_db`.`persona`
CREATE TABLE persona (
  cc INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  apellido VARCHAR(45) NOT NULL,
  genero CHAR(1) NOT NULL,
  edad INT NULL,
  CONSTRAINT PK_persona PRIMARY KEY (cc)
);

-- Table `arq_per_db`.`profesion`
CREATE TABLE profesion (
  id INT NOT NULL,
  nom VARCHAR(90) NOT NULL,
  des TEXT NULL,
  CONSTRAINT PK_profesion PRIMARY KEY (id)
);

-- Table `arq_per_db`.`estudios`
CREATE TABLE estudios (
  id_prof INT NOT NULL,
  cc_per INT NOT NULL,
  fecha DATE NULL,
  univer VARCHAR(50) NULL,
  CONSTRAINT PK_estudios PRIMARY KEY (id_prof, cc_per),
  CONSTRAINT FK_estudios_persona FOREIGN KEY (cc_per)
    REFERENCES persona (cc),
  CONSTRAINT FK_estudios_profesion FOREIGN KEY (id_prof)
    REFERENCES profesion (id)
);

-- Table `arq_per_db`.`telefono`
CREATE TABLE telefono (
  num VARCHAR(15) NOT NULL,
  oper VARCHAR(45) NOT NULL,
  duenio INT NOT NULL,
  CONSTRAINT PK_telefono PRIMARY KEY (num),
  CONSTRAINT FK_telefono_persona FOREIGN KEY (duenio)
    REFERENCES persona (cc)
);